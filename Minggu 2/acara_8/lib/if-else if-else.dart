import 'dart:io';

void main() {
  var name = "";
  var role = "";

  while (name == "") {
    print("Masukkan nama Anda: ");
    name = stdin.readLineSync()!;
    if (name == "") {
      print("Nama harus diisi!");
    }
  }

  print("Selamat datang di Dunia Warewolf, ${name}");

  while (role == "") {
    //   role == ""||
    //   role != "penyihir" ||
    //   role != "Penyihir" ||
    //   role != "guard" ||
    //   role != "Guard" ||
    //   role != "warewolf" ||
    //   role != "Warewolf"
    print("Pilih peranmu untuk memulai game: penyihir/guard/warewolf");
    role = stdin.readLineSync()!;
    if (role == "penyihir" || role == "Penyihir") {
      print(
          "Halo Penyihir ${name}, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (role == "guard" || role == "Guard") {
      print(
          "Halo Guard ${name}, kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else if (role == "warewolf" || role == "Warewolf") {
      print("Halo Werewolf ${name}, Kamu akan memakan mangsa setiap malam!");
    } else {
      print("Invalid role!");
    }
  }
}
