void main(){
    // membuat variabel dengan tipe data
    String nama = "Ellisa"; //tipe data teks
    int umur = 19;          //tipe data angka
    double tinggi = 154.43; //tipe data angka
    bool isMahasiswa = true; //tipe data boolean

    // membuat veriabel dengan kata kunci var
    var alamat = "Tuban, Jawa Timur";

    // mencetak variabel
    print("Nama saya $nama. Umur $umur tahun. Tinggi sekitar $tinggi cm.");
    print("Mahasiswa: $isMahasiswa");
    print("Alamat: $alamat");
}