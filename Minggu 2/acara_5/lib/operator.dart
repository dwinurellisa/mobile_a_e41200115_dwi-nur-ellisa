void main() {
  //operator aritmatika
  var a = 3;
  var b = 5;
  print(a + b); //penjumlahan
  print(a - b); //pengurangan
  print(a * b); //perkalian
  print(a / b); //pembagian
  print(a % b); //modulus / sisa bagi

//operator perbandingan
  var e = 3;
  var f = 5;
  print(e < f); //kurang dari
  print(e > f); //lebih dari
  print(e <= f); //kurang dari sama dengan
  print(e >= f); //lebih dari sama dengan
  print(e == f); //sama dengan
  print(e != f); //tidak sama dengan

//operator Bitwise 
  var c = 3;
  var d = 5;
  print(c & d); //and
  print(c | d); //or
  print(c ^ d); //xor
  print(~c);    //negasi
  print(c << 2); //left shift
  print(c >> 2); //right shift

}